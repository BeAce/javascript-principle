Function.prototype.apply2 = function(context) {
  const ctx = context || globalThis;
  ctx.fn = this;
  const args = [...arguments];
  let result;
  if (args.length <= 1) {
    result = ctx.fn();
  } else {
    result = ctx.fn(...args[1]);
  }

  delete ctx.fn;
  return result;
};
