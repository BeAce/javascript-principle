function new2(Fn, ...rest) {
  const obj = Object.create({});
  obj.__proto__ = Fn.prototype;

  const result = Fn.apply(obj, rest);
  if (result instanceof Object) {
    return result;
  }
  return obj;
}

module.exports = new2;