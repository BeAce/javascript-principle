# JavaScript Principle

主要用来通过最原始方式书写 JavaScript 中的方法，例如 `new` `bind` `apply` `forEach` 等。

- [bind](./bind.js)
- [call](./call.js)
- [new](./new.js)
