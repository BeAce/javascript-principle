const assert = require("assert");
const new2 = require("../new");

describe("New", () => {
  describe("inherit Person", () => {
    function Person(name) {
      this.name = name;
    }
    Person.prototype.callName = function() {
      return this.name;
    };

    const beace = new2(Person, "beace");
    it("inherit Person", () => {
      assert.equal(beace instanceof Person, true);
    });
    it("inherit Person prototype", () => {
      assert.equal(beace.callName(), "beace");
    });
  });

  describe("With return not prototype", () => {
    function Person(name) {
      this.name = name;
      return { name };
    }
    Person.prototype.callName = function() {
      return this.name;
    };
    const beace = new2(Person, "beace");
    it("catch return", () => {
      assert.deepEqual(beace, { name: "beace" });
    });
    it("can not get prototype method", () => {
      assert.deepEqual(beace.callName, undefined);
    });
  });
});
