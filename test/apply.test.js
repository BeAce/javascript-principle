const assert = require("assert");
require("../apply");

const Person = {
  name: "hello"
};

describe("Apply", () => {
  describe("call no arguments", () => {
    function Beace() {
      return this.name;
    }
    const b = Beace.apply2(Person);
    it("should return `hello`", () => {
      assert.equal(b, "hello");
    });
  });
  describe("call with arguments", () => {
    function Beace(age) {
      return `${this.name} ${age}`;
    }
    const b = Beace.apply2(Person, [24]);
    it("should return `hello 24`", () => {
      assert.equal(b, "hello 24");
    });
  });
  describe("call with new function arguments", () => {
    function Beace(age, sex) {
      return `${this.name} ${age} ${sex}`;
    }
    const b = Beace.apply2(Person, [24, "Y"]);
    it("should return `hello 24 Y`", () => {
      assert.equal(b, "hello 24 Y");
    });
  });
  describe("Same as native apply", () => {
    function Beace(age, sex) {
      return `${this.name} ${age} ${sex}`;
    }
    const b = Beace.apply2(Person, [24, "Y"]);
    it("should return `hello 24 Y`", () => {
      assert.equal(b, Beace.apply(Person, [24, "Y"]));
    });
  });
});
