require("../bind");
const assert = require("assert");

const Person = {
  name: "hello"
};

describe("Bind", () => {
  describe("bind no arguments", () => {
    function Beace() {
      return this.name;
    }
    const b = Beace.bind2(Person);
    it("should return `hello`", () => {
      assert.equal(b(), "hello");
    });
  });
  describe("bind with arguments", () => {
    function Beace(age) {
      return `${this.name} ${age}`;
    }
    const b = Beace.bind2(Person, 24);
    it("should return `hello 24`", () => {
      assert.equal(b(), "hello 24");
    });
  });
  describe("bind with new function arguments", () => {
    function Beace(age, sex) {
      return `${this.name} ${age} ${sex}`;
    }
    const b = Beace.bind2(Person, 24);
    it("should return `hello 24 Y`", () => {
      assert.equal(b("Y"), "hello 24 Y");
    });
  });
  describe("Same as native bind", () => {
    function Beace(age, sex) {
      return `${this.name} ${age} ${sex}`;
    }
    const b = Beace.bind2(Person, 24);
    const c = Beace.bind(Person, 24);
    it("should return `hello 24 Y`", () => {
      assert.equal(b("Y"), c("Y"));
    });
  });
});
