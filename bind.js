Function.prototype.bind2 = function(fn, ...rest) {
  const _this = this;
  return function() {
    return _this.apply(fn, rest.concat([...arguments]));
  };
};