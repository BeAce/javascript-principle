Function.prototype.call2 = function(context) {
  const ctx = context || globalThis;
  ctx.fn = this;
  // 为什么需要 ctx.fn 这里 ctx 是 Person this 是当前调用call2的函数
  // 上面那句话等同于
  // Person.fn = d
  // 即 Person.fn = function(detail, age) { console.log(this.name, detail, age)}
  const res = ctx.fn(...[...arguments].slice(1));
  delete ctx.fn;
  return res;
};
